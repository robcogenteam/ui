package iit.robcogen;

import iit.dsl.kinDsl.Robot;
import iit.robcogen.config.IConfigurator;
import iit.robcogen.config.ICppConfigurator;
import iit.robcogen.config.IMatlabConfigurator;
import iit.robcogen.config.properties.Configurator;
import iit.robcogen.config.properties.CppConfigurator;
import iit.robcogen.config.properties.MatlabConfigurator;
import iit.robcogen.config.properties.MaximaConfigurator;
import iit.robcogen.config.properties.PropertiesFileConfigurator;
import iit.robcogen.config.properties.TestGenConfigurator;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.eclipse.xtext.generator.JavaIoFileSystemAccess;
import org.eclipse.xtext.parser.IEncodingProvider;
import org.eclipse.xtext.resource.IResourceServiceProvider;

public class Generator {

    public static String configFileName = PropertiesFileConfigurator.defaultConfigFile;

    public static enum TargetLang {
        MAXIMA, CPP, MATLAB;
    }

    public static enum MenuItems {
        CTDSL("CTDSL", "The coordinate transforms description file .ctdsl"),

        ALLMx("ALLMx", "[All the Maxima targets]"),
        TMx("TMx", "Maxima coordinate transforms"),
        JMx("JMx", "Maxima geometric Jacobians"),

        ALLCPP("ALLC++", "[All the C++ targets]"),
        CCPP("CC++", "C++ common code"), ///<
        TCPP("TC++", "C++ coordinate Transform"),
        JCPP("JC++", "C++ geometric Jacobians"),
        DCPP("DC++", "C++ common code for dynamics"),
        IDCPP("IDC++", "C++ Inverse-Dynamics implementation"),
        FDCPP("FDC++", "C++ Forward-Dynamics implementation"),
        JSIMCPP("JSIMC++", "C++ Joint-Space-Inertia-Matrix calculation"),
        MKCPP("MKC++", "CMake file for C++ code"),

        ALLMt("ALLMt", "[All the Matlab targets]"),
        CMt("CMt", "Matlab common code"),
        TMt("TMt", "Matlab coordinate transforms"),
        JMt("JMt", "Matlab geometric Jacobians"),
        IDMt("IDMt", "Matlab Forward/Inverse-Dynamics implementation"),
        //FDMt("FDMt"),
        JSIMMt("JSIMMt", "Matlab Joint-Space-Inertia-Matrix calculation"),

        ALLSL("SL", "[All the SL targets]"),
        SLR("SLR", "SL, robot code (includes the robot model .dyn)"),
        SLU("SLU", "SL, robot-user code"),

        ALLRB("RB", "[All the robot models]"),
        RBROS("RB_ROS", "XML URDF format"),
        RBFEATH("RB_Feath", "Matlab structure for Featherstone's code"),
        RBSDFAST("RB_SDFAST", "SD/FAST model descritpion"),

        TESTS("Test", "Support code to launch the Octave tests for Octave/C++"),

        __RELOAD("Reload", "Reload the input files"),
        __QUIT("Quit", "Exit robcogen");

        public final String entry;
        public final String desc;
        public int code;
        private MenuItems(String name, String descript) {
            entry = name;
            desc = descript;
        }

        static {
            int c = 0;
            for(MenuItems item : MenuItems.values()) {
                item.code = c;
                c++;
            }
        }
    }

    public static MenuItems cppTargets[] =
        {MenuItems.CCPP,  MenuItems.TCPP,  MenuItems.JCPP,    MenuItems.DCPP,
         MenuItems.IDCPP, MenuItems.FDCPP, MenuItems.JSIMCPP, MenuItems.MKCPP};

    public static MenuItems matlabTargets[] =
        {MenuItems.TMt, MenuItems.JMt, MenuItems.CMt, MenuItems.IDMt, MenuItems.JSIMMt};

    private static JavaIoFileSystemAccess fileWriter =
            new JavaIoFileSystemAccess(
                    IResourceServiceProvider.Registry.INSTANCE,
                    new IEncodingProvider.Runtime()
                    );


    private static File robotFile = null;
    private static File transformsFile = null;

    private static void loadArgs(String[] args)
    {
        if(args.length == 0) {
            System.err.println("\nError, missing argument. Usage:");
            System.err.println("\t <generator cmd> <robot model file> [<desired transforms/Jacobians file>]\n");
            System.exit(-1);
        }

        robotFile = new File(args[0]);
        if( !robotFile.isFile() ) {
            System.err.println("Could not find robot-model file " + args[0]);
            System.exit(-1);
        }

        if(args.length > 1) {
            transformsFile = new File(args[1]);
            if( !transformsFile.isFile() ) {
                System.err.println("Could not find transforms-model file " + args[1]);
                System.exit(-1);
            }
        }
        if(args.length > 2) {
            configFileName = args[2];
        }
    }

    /**
     * This program allows to generate robot specific code using DSLs and
     * corresponding generators.
     * @param args
     */
    public static void main(String[] args)
    {
        loadArgs( args );
        Robot robot = KinDSLWrapper.getModel( robotFile );

        // Configuration files . . .

        File configFile = new File(configFileName);
        if( ! configFile.exists() ) {
            throw new RuntimeException("Could not find the configuration file " + configFileName);
        }

        Configurator configurator = new Configurator(configFile, robot);
        IConfigurator.OutputDirectories outdirConfig   = configurator;
        IConfigurator.ConfigFilesPath   cfgfilesConfig = configurator;
        MaximaConfigurator maximaConfig = new MaximaConfigurator(configFile, outdirConfig.getMaximaOutputDir());
        TestGenConfigurator testGenConfig = new TestGenConfigurator(configFile);

        File cppConfigFile = new File(cfgfilesConfig.getCPPConfigFilePath());
        if( ! cppConfigFile.exists() ) {
            throw new RuntimeException("Could not find the configuration file " + cppConfigFile.getName());
        }

        ICppConfigurator       cppConfig = new CppConfigurator   (cppConfigFile, maximaConfig, configurator.getConstantFolding());
        IMatlabConfigurator matlabConfig = new MatlabConfigurator(configFile,    maximaConfig, configurator.getConstantFolding());



        MenuItems[] codeToEnum = new MenuItems[MenuItems.values().length];
        for(MenuItems item : MenuItems.values()) {
            codeToEnum[item.code] = item;
        }
        List<MenuItems> cppTargetsList = Arrays.asList(cppTargets);
        List<MenuItems> matlabTargetsList = Arrays.asList(matlabTargets);

        KinDSLWrapper kin = new KinDSLWrapper(configurator, outdirConfig, cppConfig, matlabConfig, testGenConfig);
        kin.load(robot, transformsFile);

        Scanner scan = new Scanner(System.in);
        boolean keepAsking = true;
        MenuItems choice;
        int userIn = 0;
        while(keepAsking)
        {
            printMenu();
            System.out.println("\nWhat would you like to generate? Please enter the integer code:");

            userIn = scan.nextInt();
            if(userIn < 0 || userIn > MenuItems.__QUIT.code) {
                userIn = MenuItems.__QUIT.code;
            }
            choice = codeToEnum[userIn];

            if(cppTargetsList.contains(choice) || choice.equals(MenuItems.ALLCPP)) {
                fileWriter.setOutputPath(outdirConfig.getCPPOutputDir());
            } else if(matlabTargetsList.contains(choice) || choice.equals(MenuItems.ALLMt)) {
                fileWriter.setOutputPath(outdirConfig.getMatlabOutputDir());
            } else {
                switch(choice) {
                    case CTDSL:
                        fileWriter.setOutputPath(outdirConfig.getMiscOutputDir());
                        break;

                    case ALLMx:
                    case TMx:
                    case JMx:
                        fileWriter.setOutputPath(outdirConfig.getMaximaOutputDir());
                        break;
                    case ALLSL:
                    case SLR:
                    case SLU:
                        fileWriter.setOutputPath(outdirConfig.getSLOutputDir());
                        break;

                    case ALLRB:
                    case RBROS:
                    case RBFEATH:
                    case RBSDFAST:
                        fileWriter.setOutputPath(outdirConfig.getModelsOutputDir());
                        break;
                    case TESTS:
                        fileWriter.setOutputPath(outdirConfig.getTestOutputDir());
                        break;
                    case __RELOAD:
                        KinDSLWrapper.clearModelPool();
                        loadArgs( args );
                        robot = KinDSLWrapper.getModel( robotFile );
                        kin.load(robot, transformsFile);
                        break;
                    case __QUIT:
                        keepAsking = false;
                    default:
                }
            }
            if( ! ( choice.equals(MenuItems.__QUIT) || choice.equals(MenuItems.__RELOAD) ) )
            {
                kin.generate(fileWriter, choice);
            }
        }

        System.out.println("\n. . . Goodbye!");
        scan.close();
    }

    static void printMenu() {
        String space = " ";
        for(MenuItems item : MenuItems.values()) {
            if(item.equals(MenuItems.ALLCPP)  ||
               item.equals(MenuItems.ALLMt)   ||
               item.equals(MenuItems.ALLMx)   ||
               item.equals(MenuItems.ALLSL)   ||
               item.equals(MenuItems.ALLRB)   ||
               item.equals(MenuItems.TESTS)   ||
               item.equals(MenuItems.__RELOAD))
            {
                System.out.println();
            }
            if(item.code > 9) space = "";
            System.out.println(space + item.code + " - " + item.entry + "\t- " + item.desc);
        }
    }

}
