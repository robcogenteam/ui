package iit.robcogen.config;

import iit.dsl.kinDsl.Robot;

public interface IConfigurator {

    public interface OutputDirectories {
        public String getOutputRoot();
        public String getRobotDir(Robot robot);
        public String getCPPOutputDir();
        public String getMatlabOutputDir();
        public String getMaximaOutputDir();
        public String getSLOutputDir();
        public String getModelsOutputDir();
        public String getMiscOutputDir();
        public String getTestOutputDir();
    }

    /**
     * This interface defines getters for the full path of additional
     * configuration files required by the robotics code generator.
     */
    public interface ConfigFilesPath {
        /**
         * The path of the configuration file specific for C++
         */
        public String getCPPConfigFilePath();
    }

    public interface Misc {
        public boolean getConstantFolding();
    }
}
