package iit.robcogen.config.properties;

import java.io.File;

import iit.dsl.generator.cpp.config.IConfigurator.Names.ClassesAndTypes;
import iit.dsl.generator.cpp.kinematics.Jacobians;
import iit.dsl.generator.maxima.IConverterConfigurator;
import iit.dsl.kinDsl.Robot;
import iit.robcogen.config.ICppConfigurator;


public class CppConfigurator implements ICppConfigurator
{

    public CppConfigurator(File cppCfg, MaximaConfigurator maximaCfg, boolean cFolding)
    {
        cppNamesConfig = new CppNamesConfigurator(cppCfg);
        cppMiscConfig  = new CppMiscConfigurator(cppCfg, cFolding);
        maximaConfig   = maximaCfg;
    }

    @Override
    public iit.dsl.generator.cpp.config.IConfigurator.Paths
    getPathsConfigurator()
    {
        return cppNamesConfig;
    }

    @Override
    public iit.dsl.generator.cpp.config.IConfigurator.Names.Files
    getFileNamesConfigurator()
    {
        return cppNamesConfig;
    }

    @Override
    public iit.dsl.generator.cpp.config.IConfigurator.Names.Namespaces
    getNamespacesConfigurator()
    {
        return cppNamesConfig;
    }

    @Override
    public ClassesAndTypes getClassesAndTypesConfigurator() {
        return cppNamesConfig;
    }

    @Override
    public iit.dsl.generator.maxima.IConverterConfigurator
    getMaximaConverterConfigurator()
    {
        return maximaConfig;
    }

    @Override
    public iit.dsl.coord.generator.cpp.IConfigurator
    getTransformsDSLGeneratorConfigurator(Robot robot)
    {
        return new TransformsDSL_CppConfigurator(
                cppNamesConfig.getConfigFile(), robot,
                cppNamesConfig, maximaConfig,
                cppMiscConfig.doConstantFolding());
    }

    @Override
    public iit.dsl.generator.cpp.config.IConfigurator.Misc
    getMiscConfigurator()
    {
        return cppMiscConfig;
    }

    @Override
    public Jacobians.IConfigurator getJacobiansConfigurator()
    {
        return new Jacobians.IConfigurator()  {
            @Override
            public boolean getConstantFolding() {
                return getMiscConfigurator().doConstantFolding();
            }
            @Override
            public IConverterConfigurator getMaximaConverterConfigurator() {
                return maximaConfig;
            }
        };
    }

    private CppNamesConfigurator cppNamesConfig= null;
    private CppMiscConfigurator  cppMiscConfig = null;
    private MaximaConfigurator   maximaConfig  = null;



}
