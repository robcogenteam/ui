package iit.robcogen.config.properties;

import java.io.File;
import java.util.List;

import iit.robcogen.config.IMatlabConfigurator;
import iit.dsl.coord.coordTransDsl.Model;
import iit.dsl.coord.generator.IConstantsAccess;
import iit.dsl.coord.generator.IVariablesAccess;
import iit.dsl.generator.maxima.IConverterConfigurator;
import iit.dsl.kinDsl.Robot;





public class MatlabConfigurator extends PropertiesFileConfigurator
    implements IMatlabConfigurator
{

    public MatlabConfigurator(
            File inputFile,
            MaximaConfigurator maximaCfg,
            boolean cFolding)
    {
        super(inputFile);
        maximaConfigurator = maximaCfg;
        constantFolding = cFolding;
    }

    @Override
    public void load(
            Robot robot,
            iit.dsl.coord.coordTransDsl.Model transformsModel)
    {
        kindslConfigurator = new iit.dsl.generator.matlab.config.DefaultConfigurator(
                robot, transformsModel, maximaConfigurator );
    }

    @Override
    public IVariablesAccess getVariablesValueExprGenerator(Model model)
    {
        return kindslConfigurator.getVariablesValueExprGenerator(model);
    }

    @Override
    public IConstantsAccess getConstantsValueExprGenerator(Model model)
    {
        return kindslConfigurator.getConstantsValueExprGenerator(model);
    }

    @Override
    public List<String> getUpdateFunctionArguments()
    {
        return kindslConfigurator.getUpdateFunctionArguments();
    }
    @Override
    public List<String> getInitFunctionArguments()
    {
        return kindslConfigurator.getInitFunctionArguments();
    }

    @Override
    public iit.dsl.coord.generator.maxima.converter.IConfigurator getMaximaConverterConfigurator()
    {
        return kindslConfigurator.getMaximaConverterConfigurator();
    }

    @Override
    public IConverterConfigurator getKindslMaximaConverterConfigurator() {
        return maximaConfigurator;
    }

    @Override
    public boolean doConstantFolding() {
        return constantFolding;
    }


    private MaximaConfigurator maximaConfigurator = null;
    private iit.dsl.generator.matlab.config.IConfigurator kindslConfigurator = null;
    private boolean constantFolding = false;

}
