package iit.robcogen.config.properties;

import java.io.File;

public class CppMiscConfigurator extends PropertiesFileConfigurator
    implements iit.dsl.generator.cpp.config.IConfigurator.Misc
{
    public CppMiscConfigurator(File inputFile, boolean cFolding)
    {
        super(inputFile);
        this.constantFolding = cFolding;
    }

    @Override
    public boolean doConstantFolding() {
        return this.constantFolding;
    }

    private boolean constantFolding;
}
