package iit.robcogen.config.properties;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;


/**
 * Abstract base class for configurators that use a property file
 * to read settings.
 * A reference to the underlying config file is stored inside every Configurator.
 * Subclasses can retrieve a specific
 * org.apache.commons.configuration.Configuration object (a property reader in
 * this case) for the File with which they have been constructed.
 * Different configurators associated with the same physical file will get the
 * same property reader object.
 */
public abstract class PropertiesFileConfigurator {

    /**
     * @param inputFile The property file to be used by this configurator.
     *        If null, the class attempts to use a default one.
     * @exception A RuntimeException is thrown if the configuration file
     *            (either the parameter or the default one) cannot be loaded
     *            (e.g. does not exist).
     */
    public PropertiesFileConfigurator(File inputFile) {
            checkConfigFile(inputFile);
    }

    /**
     * @return The configuration file used by this instance.
     */
    public File getConfigFile() {
        return configFile;
    }


    protected void checkConfigFile(File inputFile) {
        if(inputFile == null) {
            log4j.warn("The File arg is null, trying to use the default one");
            configFile = new File(defaultConfigFile);
        } else {
            configFile = inputFile;
        }
        if(!configFile.exists()) {
            throw(new RuntimeException("Could not load the configuration file "
                   + configFile.getName()));
        }

        try {
        log4j.debug("The configuration file is " + configFile.getName());
        if(!(configurators.containsKey(configFile))) {
            myPropertyReader = new PropertiesConfiguration(configFile);
            ((PropertiesConfiguration)myPropertyReader).setThrowExceptionOnMissing(true);
            configurators.put(configFile, myPropertyReader);
            log4j.debug("New configurator object created.");
        }
        else {
            log4j.debug("Configurator object already instantiated.");
            myPropertyReader = getConfigurator();
        }
        } catch (ConfigurationException e) {
            throw(new RuntimeException(
                    "Fatal: cannot create a configurator for file " +
                              configFile.getName() + ". " + e.getMessage()));
        }
    }

    protected Configuration getConfigurator() {
        return(configurators.get(configFile));
    }



    private   File          configFile       = null;
    protected Configuration myPropertyReader = null;


    public static final String defaultConfigFile = "core.cfg";
    private static Logger log4j = null;
    /* A static map exploited to create a single Apache's Configuration for
     * each different property file, avoiding multiple instantiations if the
     * same file is used more than once.
     */
    private static Map<File, Configuration> configurators = null;
    static {
        log4j = Logger.getLogger(PropertiesFileConfigurator.class);
        configurators = new HashMap<File,Configuration>();
    }


}