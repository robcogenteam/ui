package iit.robcogen.config.properties;

import java.io.File;

/**
 * Configurator for Maxima-related parameters, based on properties files.
 *
 * This configurator implements two configuration interfaces related to the
 * usage of Maxima. See the corresponding documentation.
 * @author Marco Frigerio
 *
 */
public class MaximaConfigurator extends PropertiesFileConfigurator
    implements iit.dsl.generator.maxima.IConverterConfigurator,
                iit.dsl.maxdsl.utils.MaximaRunner.IConfigurator
{

    public MaximaConfigurator(File inputFile, String codepath) {
        super(inputFile);
        genCodePath = codepath;
    }

    @Override
    public String getLibsPath() {
        return myPropertyReader.getString(IDefaultKeys.Maxima.libsPath);
    }

    @Override
    public String getUtilsLibName() {
        return myPropertyReader.getString(IDefaultKeys.Maxima.libUtilsName);
    }

    @Override
    public String getTransformsLibName() {
        return myPropertyReader.getString(IDefaultKeys.Maxima.libTransformsName);
    }

    @Override
    public String getJacobiansLibName() {
        return myPropertyReader.getString(IDefaultKeys.Maxima.libJacobiansName);
    }

    @Override
    public int getLinel() {
        return myPropertyReader.getInt(IDefaultKeys.Maxima.linel);
    }

    @Override
    public int getFpprintprec() {
        return myPropertyReader.getInt(IDefaultKeys.Maxima.ffprintprec);
    }

    @Override
    public String getConversionMode() {
        return myPropertyReader.getString(IDefaultKeys.Maxima.convMode);
    }

    @Override
    public String getGeneratedCodeLocation() {
        return genCodePath;
    }


    @Override
    public iit.dsl.maxdsl.utils.MaximaRunner.IConfigurator getMaximaEngineConfigurator() {
        // This configurator implements also the
        //  iit.dsl.maxdsl.utils.MaximaRunner.IConfigurator interface!
        return this;
    }

    private String genCodePath;


}
