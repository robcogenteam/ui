package iit.robcogen.config.properties;

import iit.dsl.kinDsl.Robot;
import iit.robcogen.config.IConfigurator;

import java.io.File;

public class Configurator extends PropertiesFileConfigurator
    implements IConfigurator.OutputDirectories,
               IConfigurator.ConfigFilesPath,
               IConfigurator.Misc
{
    public Configurator(File cfgfile, Robot robot) {
        super(cfgfile);
        basepath = getOutputRoot() + File.separatorChar;

        if( myPropertyReader.getBoolean(IDefaultKeys.OutputDirectories.robotsubdir) )
        {
            basepath = basepath + getRobotDir(robot) + File.separatorChar;
        }

        // Make sure the path does not contain multiple separators, like '//'.
        // That is usually not a problem, but stupid Maxima fails when trying
        // to batch execute a path that has double separators.
        // RobCoGen will have Maxima to interpret the generated Maxima-code,
        // which is created in a subfolder of `basepath` ...
        String sep = String.valueOf(File.separatorChar);
        String bad = "[" + sep + "]+";  // a regex for any number >1 of separators
        basepath = basepath.replaceAll(bad, sep);
        //System.out.println(basepath);
    }

    @Override
    public String getOutputRoot() {
        return myPropertyReader.getString(IDefaultKeys.OutputDirectories.root);
    }

    @Override
    public String getRobotDir(Robot robot) {
        return robot.getName().toLowerCase();
    }

    @Override
    public String getCPPOutputDir() {
        return basepath+myPropertyReader.getString(IDefaultKeys.OutputDirectories.cpp);
    }

    @Override
    public String getMatlabOutputDir() {
        return basepath+myPropertyReader.getString(IDefaultKeys.OutputDirectories.matlab);
    }

    @Override
    public String getMaximaOutputDir() {
        return basepath+myPropertyReader.getString(IDefaultKeys.OutputDirectories.maxima);
    }

    @Override
    public String getSLOutputDir() {
        return basepath+myPropertyReader.getString(IDefaultKeys.OutputDirectories.SL);
    }

    @Override
    public String getMiscOutputDir() {
        return basepath+myPropertyReader.getString(IDefaultKeys.OutputDirectories.misc);
    }

    @Override
    public String getModelsOutputDir() {
        return basepath+myPropertyReader.getString(IDefaultKeys.OutputDirectories.models);
    }

    @Override
    public String getTestOutputDir() {
        return basepath+myPropertyReader.getString(IDefaultKeys.OutputDirectories.tests);
    }

    @Override
    public String getCPPConfigFilePath() {
        return myPropertyReader.getString(IDefaultKeys.ConfigFiles.cpp);
    }

    @Override
    public boolean getConstantFolding() {
        return myPropertyReader.getBoolean(IDefaultKeys.Misc.cfolding);
    }

    private String basepath;
}
