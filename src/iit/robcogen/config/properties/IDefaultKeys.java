package iit.robcogen.config.properties;

/**
 * The string constants with the default property keys to be queried for, in
 * the properties file used as configuration files.
 * This interface must be consistent with the desired keys in the configuration
 * files.
 * @author Marco Frigerio
 *
 */
public interface IDefaultKeys {

    public interface OutputDirectories {
        public static final String prefix = "generator.outdir";
        public static final String root   = prefix + ".base";
        public static final String cpp    = prefix + ".cpp";
        public static final String matlab = prefix + ".matlab";
        public static final String maxima = prefix + ".maxima";
        public static final String SL     = prefix + ".SL";
        public static final String misc   = prefix + ".misc";
        public static final String models = prefix + ".models";
        public static final String tests  = prefix + ".tests";
        public static final String robotsubdir = prefix + ".robotsubdir";
    }

    public interface Maxima {
        public static final String prefix = "generator.maxima";

        public static final String libTransformsName = prefix + ".libs.name.transforms";
        public static final String libJacobiansName  = prefix + ".libs.name.jacobians";
        public static final String libUtilsName      = prefix + ".libs.name.utils";
        public static final String libsPath    = prefix + ".libs.path";
        public static final String linel       = prefix + ".runtime.linel";
        public static final String ffprintprec = prefix + ".runtime.fpprintprec";
        public static final String convMode    = prefix + ".runtime.conversionmode";
    }


    public interface ConfigFiles {
        public static final String prefix = "generator.configfile";
        public static final String cpp = prefix + ".cpp";
    }

    public interface CPP {
        public static final String header_install_path = "cpp.filename.headerInstallPath";
        public static final String outfolder      = "cpp.filename.folder";
        public static final String h_declarations = "cpp.filename.header.declarations";
        public static final String h_linkDataMap  = "cpp.filename.header.linkdatamap";
        public static final String h_jointDataMap = "cpp.filename.header.jointdatamap";
        public static final String h_transforms   = "cpp.filename.header.transforms";
        public static final String h_parameters   = "cpp.filename.header.parameters";
        public static final String h_jacobians    = "cpp.filename.header.jacobians";
        public static final String h_inertias     = "cpp.filename.header.inertia";
        public static final String h_inertiaParams= "cpp.filename.header.dyn_params";
        public static final String h_invdyn       = "cpp.filename.header.inv_dyn";
        public static final String h_fwddyn       = "cpp.filename.header.fwd_dyn";
        public static final String h_jsim         = "cpp.filename.header.jsim";
        public static final String src_transforms = "cpp.filename.source.transforms";
        public static final String src_jacobians  = "cpp.filename.source.jacobians";
        public static final String src_inertias   = "cpp.filename.source.inertia";
        public static final String src_invdyn     = "cpp.filename.source.inv_dyn";
        public static final String src_fwddyn     = "cpp.filename.source.fwd_dyn";
        public static final String src_jsim       = "cpp.filename.source.jsim";
        public static final String ns_enclosing   = "cpp.namespace.enclosing";
        public static final String ns_robot       = "cpp.namespace.robot";
        public static final String ns_iit_rbd     = "cpp.namespace.iit_rbd";
        public static final String type_T6D_motion   = "cpp.types.transforms.spatial_motion";
        public static final String type_T6D_force    = "cpp.types.transforms.spatial_force";
        public static final String type_THomogeneous = "cpp.types.transforms.homogeneous";
    }

    public interface Misc {
        public static final String prefix = "generator.misc";
        public static final String cfolding = prefix + ".constantfolding";
    }
}
