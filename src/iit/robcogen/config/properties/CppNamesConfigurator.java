package iit.robcogen.config.properties;

import iit.dsl.kinDsl.Robot;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * Configurator for the generation of C++ code.
 *
 * See the documentation of the implemented interfaces and of the base class,
 * for details.
 *
 * This implementation overrides most of the default implementation of the base
 * class, and returns values read from a properties file.
 *
 * @author Marco Frigerio
 */
public class CppNamesConfigurator extends PropertiesFileConfigurator
    implements
    iit.dsl.generator.cpp.config.IConfigurator.Names.Files,
    iit.dsl.generator.cpp.config.IConfigurator.Names.Namespaces,
    iit.dsl.generator.cpp.config.IConfigurator.Names.ClassesAndTypes,
    iit.dsl.generator.cpp.config.IConfigurator.Paths
{

    public CppNamesConfigurator(File inputFile) {
        super(inputFile);
        defaultConfig = new iit.dsl.generator.cpp.config.DefaultConfigurator();
    }

    //////////
    // PATH //
    //////////

    @Override
    public String maximaCodeTransforms() {
        return myPropertyReader.getString(IDefaultKeys.OutputDirectories.maxima);
    }

    @Override
    public String maximaCodeJacobians() {
        return myPropertyReader.getString(IDefaultKeys.OutputDirectories.maxima);
    }

    @Override
    public String maximaLibs() {
        return myPropertyReader.getString(IDefaultKeys.Maxima.libsPath);
    }




    ////////////////
    // NAMESPACES //
    ////////////////
    @Override
    public List<String> iit_rbd() {
        String[] ret = myPropertyReader.getStringArray(IDefaultKeys.CPP.ns_iit_rbd);
        return Arrays.asList(ret);
    }


    @Override
    public List<String> enclosing() {
        String[] ret = myPropertyReader.getStringArray(IDefaultKeys.CPP.ns_enclosing);
        return Arrays.asList(ret);
    }



    @Override
    public String robot(Robot r) {
        return defaultConfig.robot(r);
    }




    ////////////////////////
    // CLASSES AND TYPES  //
    ////////////////////////

    @Override
    public String transforms_homogeneous() {
        return myPropertyReader.getString(IDefaultKeys.CPP.type_THomogeneous);
    }

    @Override
    public String transforms_spatial_motion() {
        return myPropertyReader.getString(IDefaultKeys.CPP.type_T6D_motion);
    }

    @Override
    public String transforms_spatial_force() {
        return myPropertyReader.getString(IDefaultKeys.CPP.type_T6D_force);
    }


    ////////////
    // FILES  //
    ////////////

    @Override
    public String folder(Robot robot) {
        return defaultConfig.folder(robot);
    }

    @Override
    public List<String> headerInstallPath(Robot robot) {
        String[] ret = myPropertyReader.getStringArray(IDefaultKeys.CPP.header_install_path);
        return Arrays.asList(ret);
    }



    @Override
    public String h_declarations(Robot robot) {
        return myPropertyReader.getString(IDefaultKeys.CPP.h_declarations);
    }




    @Override
    public String h_linkDataMap(Robot robot) {
        return myPropertyReader.getString(IDefaultKeys.CPP.h_linkDataMap);
    }




    @Override
    public String h_jointDataMap(Robot robot) {
        return myPropertyReader.getString(IDefaultKeys.CPP.h_jointDataMap);
    }



    @Override
    public String h_transforms(Robot robot) {
        return myPropertyReader.getString(IDefaultKeys.CPP.h_transforms);
    }

    @Override
    public String h_parameters(Robot robot) {
        return myPropertyReader.getString(IDefaultKeys.CPP.h_parameters);
    }

    @Override
    public String h_jacobians(Robot robot) {
        return myPropertyReader.getString(IDefaultKeys.CPP.h_jacobians);
    }



    @Override
    public String h_inertias(Robot r) {
        return myPropertyReader.getString(IDefaultKeys.CPP.h_inertias);
    }
    @Override
    public String h_mass_parameters(Robot robot) {
        return myPropertyReader.getString(IDefaultKeys.CPP.h_inertiaParams);
    }



    @Override
    public String h_invdyn(Robot r) {
        return myPropertyReader.getString(IDefaultKeys.CPP.h_invdyn);
    }




    @Override
    public String h_fwddyn(Robot r) {
        return myPropertyReader.getString(IDefaultKeys.CPP.h_fwddyn);
    }




    @Override
    public String h_jsim(Robot r) {
        return myPropertyReader.getString(IDefaultKeys.CPP.h_jsim);
    }




    @Override
    public String src_transforms(Robot r) {
        return myPropertyReader.getString(IDefaultKeys.CPP.src_transforms);
    }




    @Override
    public String src_jacobians(Robot r) {
        return myPropertyReader.getString(IDefaultKeys.CPP.src_jacobians);
    }




    @Override
    public String src_inertias(Robot r) {
        return myPropertyReader.getString(IDefaultKeys.CPP.src_inertias);
    }




    @Override
    public String src_invdyn(Robot r) {
        return myPropertyReader.getString(IDefaultKeys.CPP.src_invdyn);
    }




    @Override
    public String src_fwddyn(Robot r) {
        return myPropertyReader.getString(IDefaultKeys.CPP.src_fwddyn);
    }




    @Override
    public String src_jsim(Robot r) {
        return myPropertyReader.getString(IDefaultKeys.CPP.src_jsim);
    }

    private iit.dsl.generator.cpp.config.DefaultConfigurator defaultConfig = null;



}
