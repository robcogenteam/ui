package iit.robcogen.config.properties;

import java.io.File;

import java.util.List;


import iit.dsl.coord.coordTransDsl.Model;
import iit.dsl.coord.generator.Utilities.MatrixType;
import iit.dsl.coord.generator.cpp.IConstantsAccess;
import iit.dsl.coord.generator.cpp.IVariablesAccess;
import iit.dsl.kinDsl.Robot;


public class TransformsDSL_CppConfigurator extends PropertiesFileConfigurator
    implements iit.dsl.coord.generator.cpp.IConfigurator
{
    public TransformsDSL_CppConfigurator(
            File propertiesFile,
            Robot robot,
            iit.dsl.generator.cpp.config.IConfigurator.Paths pathConfig,
            iit.dsl.coord.generator.maxima.converter.IConfigurator maximaConverterConfig,
            boolean constantFolding)
    {
        super(propertiesFile);
        defaultConfig = new iit.dsl.generator.cpp.config.TransformsGeneratorConfigurator(robot, pathConfig, constantFolding);
        maximaConverterConfigurator = maximaConverterConfig;

    }

    @Override
    public String headerFileName(Model model) {
        return myPropertyReader.getString(IDefaultKeys.CPP.h_transforms);
    }

    @Override
    public List<String> enclosingNamespaces(Model model) {
        return defaultConfig.enclosingNamespaces(model);
    }

    @Override
    public CharSequence includeDirectives(Model model) {
        return defaultConfig.includeDirectives(model);
    }

    @Override
    public String localTypeName(MatrixType matrixtype) {
        return defaultConfig.localTypeName(matrixtype);
    }

    @Override
    public IVariablesAccess getVariablesValueExprGenerator(Model model) {
        return defaultConfig.getVariablesValueExprGenerator(model);
    }

    @Override
    public IConstantsAccess getConstantsValueExprGenerator(Model model) {
        return defaultConfig.getConstantsValueExprGenerator(model);
    }

    @Override
    public String matrixBaseType(Model model, MatrixType transformtype) {
        return defaultConfig.matrixBaseType(model, transformtype);
    }

    @Override
    public iit.dsl.coord.generator.maxima.converter.IConfigurator
    getMaximaConverterConfigurator()
    {
        return maximaConverterConfigurator;
    }

    @Override
    public String paramsHeaderFileName(Model model) {
        return defaultConfig.paramsHeaderFileName(model); // TODO read with the configurator
    }

    @Override
    public String className(MatrixType matrixtype) {
        return defaultConfig.className(matrixtype);
    }

    @Override
    public String scalarType() {
        return defaultConfig.scalarType();
    }
    @Override
    public String sinFunctionName() {
        return defaultConfig.sinFunctionName();
    }
    @Override
    public String cosFunctionName() {
        return defaultConfig.cosFunctionName();
    }


    private iit.dsl.generator.cpp.config.TransformsGeneratorConfigurator defaultConfig = null;
    private iit.dsl.coord.generator.maxima.converter.IConfigurator maximaConverterConfigurator = null;



}
