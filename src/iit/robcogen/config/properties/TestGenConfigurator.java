package iit.robcogen.config.properties;

import java.io.File;
import iit.dsl.kinDsl.Robot;


public class TestGenConfigurator extends PropertiesFileConfigurator
                       implements iit.dsl.generator.TestsGenerator.IConfigurator
{
    public TestGenConfigurator(File cfgFile)
    {
        super(cfgFile);
    }

    @Override
    public String octaveTestsSrcRoot() {
        return "octave-tests";
    }

    @Override
    public String genOctaveDir(Robot robot) {
        return "../"+myPropertyReader.getString(IDefaultKeys.OutputDirectories.matlab);
    }

    @Override
    public String cppTestsSrcDir(Robot robot) {
        return ".";
    }

    @Override
    public String genModelsDir(Robot robot) {
        return "../"+myPropertyReader.getString(IDefaultKeys.OutputDirectories.models);
    }

    @Override
    public String octaveStartupScriptName() {
        return "octave-startup.m";
    }

}
