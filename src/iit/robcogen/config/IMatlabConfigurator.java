package iit.robcogen.config;

import iit.dsl.kinDsl.Robot;



public interface IMatlabConfigurator extends iit.dsl.generator.matlab.config.IConfigurator
{
    /**
     *
     * @param robot
     * @param transformsModel
     */
    public void load(Robot robot,
            iit.dsl.coord.coordTransDsl.Model transformsModel);
}
