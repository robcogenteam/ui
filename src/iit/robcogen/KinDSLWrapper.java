package iit.robcogen;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;

import iit.dsl.KinDslStandaloneSetup;
import iit.dsl.TransSpecsAccessor;
import iit.dsl.generator.TestsGenerator;
import iit.dsl.generator.common.GeometryModels;
import iit.dsl.generator.common.TransformsDSLsUtils;
import iit.dsl.kinDsl.Robot;
import iit.robcogen.Generator.MenuItems;
import iit.robcogen.config.IConfigurator;
import iit.robcogen.config.ICppConfigurator;
import iit.robcogen.config.IMatlabConfigurator;

/**
 * Wrapper around the code generators of the Kinematics DSL.
 * @author Marco Frigerio
 *
 */
public class KinDSLWrapper
{
    private static XtextResourceSet resourceSet = null;
    private static Resource resource = null;
    private static final Injector injector = new KinDslStandaloneSetup().createInjectorAndDoEMFRegistration();;

    static {
        resourceSet = injector.getInstance(XtextResourceSet.class);
        resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
    }

    public static void clearModelPool() {
        resourceSet.getResources().clear();
    }

    /**
     *
     * @param modelFile the File with the robot-model description compliant with the
     *        Kinematics DSL
     * @return the Robot instance corresponding to the given document, created by
     *         the DSL machinery
     */
    public static Robot getModel(File modelFile) {
        if( !modelFile.exists() ) {
            throw new RuntimeException("Could not find file " + modelFile.getName());
        }

        resource = resourceSet.getResource(URI.createURI(modelFile.toURI().toString()), true);
        List<Resource.Diagnostic> errors = resource.getErrors();
        if(errors.size() > 0) {
            StringBuffer msg = new StringBuffer();
            msg.append("\nErrors while loading the document ");
            msg.append(modelFile.getName() + "\n");
            for(Resource.Diagnostic err : errors) {
                msg.append("\n\t " + err.getMessage() +
                        " (line " + err.getLine() + ")\n");
            }
            throw new RuntimeException(msg.toString());
        }
        return (Robot)resource.getContents().get(0);
    }


    private final IConfigurator.Misc miscCfg;
    private final IConfigurator.OutputDirectories outdirsCfg;
    private ICppConfigurator    cppConfigurator;
    private IMatlabConfigurator matlabConfigurator = null;

    private Robot robot = null;
    private final iit.dsl.generator.misc.Misc genMisc;
    private final iit.dsl.generator.maxima.Generator genMaxima;
    private final iit.dsl.generator.cpp.Generator genCpp;
    private final iit.dsl.generator.matlab.Generator genMatlab;
    private final iit.dsl.generator.sl.Generator genSL;
    private final iit.dsl.generator.TestsGenerator genTests;

    private GeometryModels geometry = null;
    private iit.dsl.transspecs.transSpecs.DesiredTransforms desTransformsModel = null;
    private iit.dsl.coord.coordTransDsl.Model transformsModel = null;
    private CharSequence transformsDSLDocument = null;

    private TransformsDSLsUtils transformsDSLsUtils  = null;


    public static enum MiscTargets{
        FEATH_MATLAB, URDF;
    }
    public static enum RBDTargets {
        COMMON, FWD_DYN, INV_DYN, JSIM;
    }

    public static enum SLTargets {
        ROBOT, USER;
    }

    public KinDSLWrapper(
            IConfigurator.Misc miscCf,
            IConfigurator.OutputDirectories dirCfg,
            ICppConfigurator    cppConfig,
            IMatlabConfigurator matlabConfig,
            TestsGenerator.IConfigurator testCfg)
    {
        miscCfg    = miscCf;
        outdirsCfg = dirCfg;
        cppConfigurator    = cppConfig;
        matlabConfigurator = matlabConfig;

        genMisc   = iit.dsl.generator.misc.Misc.getInstance();
        genMaxima = new iit.dsl.generator.maxima.Generator();
        genCpp    = new iit.dsl.generator.cpp.Generator(cppConfigurator);
        genMatlab = new iit.dsl.generator.matlab.Generator(matlabConfig);
        genSL     = new iit.dsl.generator.sl.Generator();
        genTests  = new iit.dsl.generator.TestsGenerator(testCfg);

        transformsDSLsUtils = new TransformsDSLsUtils();
    }


    public void load(Robot rob, File transforms)
    {
        robot    = rob;
        geometry = new GeometryModels(robot, miscCfg.getConstantFolding());

        if(transforms != null) {
            desTransformsModel = new TransSpecsAccessor().getDesiredTransforms(transforms);
        } else {
            desTransformsModel = null;
        }

        iit.dsl.generator.common.TransformsDSLsUtils.checkConsistency(robot, desTransformsModel);

        try {
            // TODO dont do anything if it has been already done for the same robot

            desTransformsModel    = transformsDSLsUtils.addDefaultTransforms(robot, desTransformsModel);
            transformsDSLDocument = geometry.getTransformsDoc(desTransformsModel);
            transformsModel       = geometry.getTransformsModel(desTransformsModel);

            matlabConfigurator.load(rob, transformsModel);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public void generate(IFileSystemAccess fsa, MenuItems... targets){
        if(robot == null) {
            return;
            //TODO log warning
        }
        for(MenuItems which : targets) {
        switch(which) {
            case CTDSL:
                fsa.generateFile(robot.getName()+".motdsl", geometry.getMotionsDoc());
                    fsa.generateFile(TransformsDSLsUtils.documentDefaultName_TransformsDSL(robot),
                            transformsDSLDocument);
                break;

            case ALLCPP:
                generate(fsa, Generator.cppTargets);
                break;
            case CCPP:
                genCpp.generateCommons(robot, fsa);
                break;
            case TCPP:
                genCpp.generateTransforms(robot, fsa, transformsModel);
                break;
            case JCPP:
                if(maximaJacobianFileExists(robot)) {
                    genCpp.generateJacobiansFiles(robot, fsa, desTransformsModel, transformsModel);
                }
                break;
            case DCPP:
                genCpp.generateLinkInertias(robot, fsa);
                break;
            case IDCPP:
                genCpp.generateInverseDynamicsStuff(robot, fsa, transformsModel);
                break;
            case  FDCPP:
                genCpp.generateForwardDynamicsStuff(robot, fsa, transformsModel);
                break;
            case JSIMCPP:
                genCpp.generateInertiaMatrixStuff(robot, fsa, transformsModel);
                break;
            case  MKCPP:
                genCpp.generateMakefiles(robot, fsa);
                break;

            case ALLMt :
                generate( fsa, Generator.matlabTargets);
                break;
            case TMt   :
                genMatlab.generateTransformsFiles(robot, fsa, transformsModel);
                break;
            case JMt   :
                if(maximaJacobianFileExists(robot)) {
                    genMatlab.generateJacobiansFiles(robot, fsa, desTransformsModel, transformsModel);
                }
                break;
            case CMt   :
                if( ! matlabConfigurator.doConstantFolding() ) {
                    genMatlab.generateConstantsFile(robot, fsa);
                }
                genMatlab.generateParametersFile(robot, fsa);
                genMatlab.generateCommonDynamicsFiles(robot, transformsModel, fsa);
                break;
            case IDMt  :
                genMatlab.generateIDFiles(robot, transformsModel, fsa);
                genMatlab.generateFDFiles(robot, transformsModel, fsa);
                break;
            case JSIMMt:
                genMatlab.generateJSIMFiles(robot, transformsModel, fsa);
                break;

            case ALLMx:
                generate(fsa, MenuItems.TMx, MenuItems.JMx);
                break;
            case TMx:
                genMaxima.generateTransformsSources(robot, fsa, transformsModel);
                break;
            case JMx:
                genMaxima.generateJacobiansSources(robot, fsa, desTransformsModel, transformsModel);
                break;

            case ALLSL:
                generate( fsa, MenuItems.SLR, MenuItems.SLU);
                break;
            case SLR:
                genSL.generateRobotFiles(robot, transformsModel, cppConfigurator.getMiscConfigurator().doConstantFolding(), fsa);
                break;
            case SLU:
                genSL.generateRobotUserFiles(robot, fsa);
                break;

            case ALLRB:
                generate(fsa, MenuItems.RBROS, MenuItems.RBFEATH, MenuItems.RBSDFAST);
                break;
            case RBROS:
                fsa.generateFile(robot.getName()+".urdf", genMisc.URDF_ROS_model(robot));
                break;
            case RBFEATH:
                fsa.generateFile(robot.getName() + ".m", genMatlab.featherstoneMatlabModel(robot));
                break;
            case RBSDFAST:
                fsa.generateFile(robot.getName() + ".sd", genMisc.SDFAST_model(robot));
                break;
            case TESTS:
                genCpp.generateCmdLineTests(robot, fsa);
                fsa.generateFile(genTests.getConfigurator().octaveStartupScriptName(), genTests.octaveStartup(robot));
                fsa.generateFile("run-octave-tests.sh", genTests.testExecShellScript(robot));
                break;
            default:
                throw new RuntimeException("Unknown target to be generated");
        }
        }
    }


    private boolean maximaJacobianFileExists(Robot robot) {
        String robotsMaximaFolder = outdirsCfg.getMaximaOutputDir();
        String maximaJacFilePath  = robotsMaximaFolder + "/" +
                iit.dsl.generator.maxima.Jacobians.fileName(robot);
        File maximaJacFile = new File(maximaJacFilePath);
        if( !maximaJacFile.exists() ) {
            System.err.println("Cannot generate other source code for the Jacobian without " +
                    "the corresponding Maxima code (" + maximaJacFilePath + "). Generate it first" +
                    " (robot " + robot.getName() + ").");
            return false;
        }
        return true;
    }

}
