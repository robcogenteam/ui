# Introduction

This repository contains the source code of the RobCoGen-frontend, the simple,
textual user-interface of RobCoGen (the Robotics Code Generator).

More information about RobCoGen can be found in the
[website](https://robcogenteam.bitbucket.io).

This repository contains the Java source code of the frontend, a template
for configuration files required by RobCoGen at runtime, a license file and
this readme.

# Building

The robcogen-frontend is not a standalone project but a component of RobCoGen,
and it has to be built along with the other components.

For building instructions please refer to the documentation of the
[robcogen-building project](https://bitbucket.org/robcogenteam/robcogen-building).

# Copyright notice

The RobCoGen-frontend is part of the RobCoGen project.

Copyright (c) 2015 2016 2017 2018 Marco Frigerio
All rights reserved.

See the license file for additional information.
